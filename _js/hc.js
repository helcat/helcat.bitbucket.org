
// Smooth-Scroll plugin for nav
$(document).ready(function() {
  navIndicator();
  smoothScroll();
  accordionInit();
  });

function navIndicator() {
  $('.navLink').click(function() {
    $('.navLink').removeClass('active');
    $(this).addClass('active', 0, 'easeInExpo');
  });
};

function smoothScroll() {
  $('#header').smoothScroll({
      delegateSelector: 'a'
    });
};

function accordionInit() {
  $( "#accordion" ).accordion({
     collapsible: true,
     active: false,
     heightStyle: "content",
   });
 };
